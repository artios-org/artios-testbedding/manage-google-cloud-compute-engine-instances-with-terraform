## The provider to use for the cloud resources
# https://registry.terraform.io/providers/hashicorp/google/latest/docs
provider "google" {
  project     = var.gcp_project_id
  credentials = file("${var.gce_service_account_key_file_path}")
}

## Create a single Compute Engine instance
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance
resource "google_compute_instance" "artio-nodes" {
  # Number of instances to create
  count = 3
  # Name given to the Compute Engine instance
  name = "artio-node-${count.index + 1}"
  # Type of the Compute Engine instance
  machine_type = "e2-micro"
  # Zone of the Compute Engine instance
  zone = "us-east1-b"

  # The metadata associated to the Compute Engine instance
  metadata = {
    # The SSH keys to generate to access the Compute Engine instance
    ssh-keys = "${var.gce_root_username}:${file("${var.gce_root_ssh_pub_key_file_path}")}"
  }

  boot_disk {
    initialize_params {
      # https://cloud.google.com/compute/docs/images
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Include this section to give the VM an external IP address
    }
  }
}

## Add a firewall rule to allow HTTP/HTTPS access to the instance
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall
resource "google_compute_firewall" "http-and-https" {
  name          = "allow-http-and-https"
  network       = "default"
  source_ranges = ["0.0.0.0/0"]
  allow {
    ports    = ["80", "443"]
    protocol = "tcp"
  }
}
