## The backend to use to store the Terraform state
# https://www.terraform.io/language/settings/backends
# https://www.terraform.io/language/settings/backends/local
# https://www.terraform.io/language/settings/backends/http
# https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html
terraform {
  backend "local" {
  }
}
