## Variables
# https://learn.hashicorp.com/tutorials/terraform/variables
variable "gce_root_ssh_pub_key_file_path" {
  description = "The path to the public SSH key to access the Google Compute Engine instance"
  type        = string
  nullable    = false
}

variable "gce_root_username" {
  description = "The root user to create on the Google Compute Engine instance"
  type        = string
  nullable    = false
}

variable "gce_service_account_key_file_path" {
  description = "The path to the Google Cloud Compute Engine service account key to manage Google Cloud ressources"
  type        = string
  nullable    = false
}

variable "gcp_project_id" {
  description = "The ID of the Google Cloud project"
  type        = string
  nullable    = false
}
