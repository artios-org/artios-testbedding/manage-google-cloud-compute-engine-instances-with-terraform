## Output variables
# https://learn.hashicorp.com/tutorials/terraform/outputs
output "artio_nodes_ip" {
  value = google_compute_instance.artio-nodes[*].network_interface.0.access_config.0.nat_ip
}
