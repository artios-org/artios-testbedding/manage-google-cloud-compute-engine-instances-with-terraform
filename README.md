# Manage Google Cloud Compute Engine instances with Terraform

This guide will request instances (also called "virtual machines (VMs)") on Google Cloud's Compute Engine (GCE) that will be managed by Terraform.

## Relative work

- _None at the moment._

## Prerequisites

- [Google Cloud CLI (gcloud)](https://cloud.google.com/sdk/docs/install-sdk) must be installed.
- [Terraform](https://www.terraform.io/downloads) must be installed.

## Guide

_Highly inspired by the [Get started with Terraform](https://cloud.google.com/docs/terraform/get-started-with-terraform) and [GitLab-managed Terraform state](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html) guides._

### Create the directories structure

At the root level of your workspace, create two directories:

- [`credentials`](./credentials/): Store the credentials information
- [`terraform`](./terraform/): Store the Terraform configuration

Create the [`.gitignore`](./credentials/.gitignore) file to avoid credentials leaks.

### Create the Google Cloud Project

Create a Google Cloud Project by going to the [Google Cloud Console](https://console.cloud.google.com), select **Select a project** in the upper right corner of the screen and select **New project**. Name your project (_artios-testbedding-terraform_ - Note: the name is unique for all projects on Google Cloud, you might need to choose another one while following this guide), select a Billing account, select a Location and select **Create**. Save the project ID, it will be used later.

### Create the Service account and its key

[Enable the Compute Engine API](https://console.cloud.google.com/flows/enableapi?apiid=compute.googleapis.com).

Create a Google Service Account by going to **IAM & Admin > Service accounts** on the left sidebar. Select **Create service account**, name the Service Account Key (_artios-testbedding-terraform_) and select **Create and continue**. Grant the _Compute Admin_ to the Service account access for the project and select **Continue**. Select **Done**. Select the newly created service account, select **Keys** and add a new key (JSON format). Save the key under the name `gce-service-account-key.json` in the [`credentials`](./credentials/) directory.

### Login to Google Cloud with the command line tool

Locally, login to Google Cloud using the [Google Cloud CLI (gcloud)](https://cloud.google.com/sdk/docs/install-sdk) and select the project.

```sh
# Initialize and login to Google Cloud
gcloud init

# List all available projects
gcloud projects list

# Select the `artios-testbedding-terraform` project
gcloud config set project <id of the gcp project>
```

### Generate the private/public SSH keys pair

Generate the private/public SSH keys pair that will be used to access the instances in the [`credentials`](./credentials/) directory.

```sh
# Create a new pair of SSH keys
ssh-keygen \
    -t ed25519 \
    -f ./credentials/gce-root-ssh-key \
    -q \
    -N "" \
    -C ""
```

### Create the Terraform configuration

Create the Terraform configuration files in the [`terraform`](./terraform/) directory.

Create the [`.gitignore`](./terraform/.gitignore) file.

Create the [`backend.tf`](./terraform/backend.tf) file.

Create the [`main.tf`](./terraform/main.tf) file.

Create the [`outputs.tf`](./terraform/outputs.tf) file.

Create the [`variables.tf`](./terraform/variables.tf) file.

Create a `terraform.tfvars` file with the following content (edit where needed):

```sh
gce_root_ssh_pub_key_file_path    = "../credentials/gce-root-ssh-key.pub"
gce_root_username                 = "CHANGE_ME"
gce_service_account_key_file_path = "../credentials/gce-service-account-key.json"
gcp_project_id                    = "CHANGE_ME"
```

### Manage the infrastructure locally

_The following commands are run in the [`terraform`](./terraform/) directory._

Initiliaze the Terraform state.

```sh
# Initialize the Terraform state
terraform init
```

Validate the Terraform state.

```sh
# Check the Terraform configuration
terraform validate
```

Create an execution plan to preview the changes that will be made to the infrastructure and save it locally.

```sh
# Create the execution plan
terraform plan -input=false -out=.terraform/plan.cache
```

If satisfied with the execution plan, apply it.

```sh
# Apply the execution plan
terraform apply -input=false .terraform/plan.cache
```

If no errors occur, you have successfully managed to request the instances on Google Cloud using Terraform. You should see the IPs of the Google Compute instances in the console.

You can now destroy the infrastructure.

```sh
# Destroy the infrastructure
terraform destroy
```

### Manage the infrastructure with GitLab CI/CD

Terraform has successfully been used locally to manage our infrastructure. We will now use GitLab CI/CD to manage our infrastructure with the rest of the team so anyone can make changes to the infrastructure.

Create the [`.gitlab-ci.yml`](./.gitlab-ci.yml) file.

Transform the content of credentials files into base64 and save the output of the commands, it will be used later.

```sh
# Transform the public SSH key to base64
base64 ./credentials/gce-root-ssh-key.pub

# Transform the service account key to base64
base64 ./credentials/gce-service-account-key.json
```

[Add the CI/CD environment variables to GitLab CI/CD](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) by going to the GitLab project. On the left sidebar, select **Settings > CI/CD**. Select **Variables** and add the following variables (mask and protect all variables for security reasons):

- `GCE_ROOT_SSH_PUBLIC_KEY`: The content in base64 of the `gce-root-ssh-key.pub` file
- `GCE_ROOT_USERNAME`: The root username of the instance
- `GCE_SERVICE_ACCOUNT_KEY`: The content in base64 of the `gce-service-account-key.json` file
- `GCP_PROJECT_ID`: The ID of the Google Cloud project

Push the changes to GitLab. A [pipeline](https://gitlab.com/artios-org/artios-testbedding/manage-google-cloud-compute-engine-instances-with-terraform/-/pipelines) should start with the following jobs:

- `terraform-init`: Initialize Terraform.
- `terraform-validate`: Validate the Terraform configuration.
- `terraform-format`: Check if all Terraform files are correctly formatted.
- `terraform-plan`: Create the execution plan.
- `terraform-apply`: Apply the execution plan and create the infrastructure using the execution plan. This job is manual.
- `terraform-destroy`: Destroy the infrastructure using the execution plan. This job is manual.

If no errors occur, you have successfully managed to request the instances on Google Cloud using Terraform and GitLab CI/CD.

## Additional resources

- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) - GitLab CI/CD is a tool for software development using the continuous methodologies: Continuous Integration (CI), Continuous Delivery (CD) and Continuous Deployment (CD).
- [Google Cloud Platform](https://cloud.google.com/) - Meet your business challenges head on with cloud computing services from Google, including data management, hybrid & multi-cloud, and AI & ML.
- [Terraform](https://www.terraform.io/) - Terraform is an open-source infrastructure as code software tool that enables you to safely and predictably create, change, and improve infrastructure.

## License

This work is licensed under the [Non-Profit Open Software License version 3.0](https://opensource.org/licenses/NPOSL-3.0).
